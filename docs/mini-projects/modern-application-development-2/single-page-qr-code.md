# Single Page QR Code Generator App


## Problem
- You have to generate a QR code for a given text
- Have a text box to enter the text
- As the text is entered, create/update the QR code live
- Give a button to download the image

## Solution Ideas
- Use [qrcode.js](https://davidshimjs.github.io/qrcodejs/) to actually create the QR code
- Create a text box and tie it to an image that shows qr code
- As the text changes, create and update the image
